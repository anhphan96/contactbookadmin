import {ChildrenInfoClass} from "./children-info-class";

export class Parents {
  id: string;

  name: string;

  birthDay: Date;

  gender: string;

  phone: string;

  userName: string;

  password: string;

  kc_Id: string;

  isEditing: boolean;

  childrenInfoOfClasses: ChildrenInfoClass[];
}
