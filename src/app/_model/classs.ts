import {TeacherInfoClass} from "./teacher-info-class";
import {ChildrenInfoClass} from "./children-info-class";

export class Classs {
  id: string;

  name: string;

  active: boolean;

  teacherInfoOfClasses: TeacherInfoClass[];

  childrenInfoOfClasses: ChildrenInfoClass[];

  endedAt: Date;

  createdAt: Date;
}
