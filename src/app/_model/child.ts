import {ClassInfo} from "./class-info";

export class Child {
  id: string;

  name: string;

  birthDay: Date;

  age: number;

  gender: string;

  parentsId: string;

  classInfos: ClassInfo[];

  currentClassId: string;

  currentClassName: string;
}
