export class ClassInfo {
  classId: string;
  from: Date;
  to: Date;
  className: string;
}
