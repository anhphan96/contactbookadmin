import {ClassInfo} from "./class-info";

export class Teacher {
  id: string;

  name: string;

  birthday: Date;

  gender: string;

  phone: string;

  classInfos: ClassInfo[];

  currentClassId: string;

  currentClassName: string;

  userName: string;

  password: string;

  kc_Id: string;

  isEditing: boolean;
}
