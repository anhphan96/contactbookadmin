import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from "rxjs/index";
import {HttpService} from "./http.service";
import {AlertService} from "./alert.service";
import {BASE_CHILDREN} from "../_constant/path.constant";
import {Child} from "../_model/child";
import {map, catchError} from "rxjs/internal/operators";

@Injectable({
  providedIn: 'root'
})
export class ChildrenService implements Resolve<any> {
  children: Child[];
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.getData();
  }

  constructor(private httpService: HttpService,
              private alertService: AlertService,
              private router: Router) {
  }

  public getChildren(): Observable<any> {
    return this.httpService.get(BASE_CHILDREN);
  }

  public saveChild(child: Child) {
    return this.httpService.post(BASE_CHILDREN, child);
  }

  private getData() {
    this.alertService.alertLoading('Loading...');
    return this.getChildren().pipe(
      map((data) => {
        this.alertService.closeAlert();
        this.children = data;
      }),
      catchError(err => {
        this.alertService.closeAlert();
        this.router.navigate(['/admin']);
        return of(err);
      })
    );
  }
}
