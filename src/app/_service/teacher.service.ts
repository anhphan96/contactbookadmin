import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs/index';
import {BASE_TEACHER, BASE_TEACHER_ACCOUNT} from "../_constant/path.constant";
import {HttpService} from "./http.service";
import {AlertService} from "./alert.service";
import {catchError, map} from "rxjs/internal/operators";
import {Teacher} from "../_model/teacher";

@Injectable({
  providedIn: 'root'
})
export class TeacherService implements Resolve<any> {
  teachers: Teacher[];

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.getData();
  }

  constructor(private httpService: HttpService,
              private alertService: AlertService,
              private router: Router) {
  }

  public getTeachers(): Observable<any> {
    return this.httpService.get(BASE_TEACHER);
  }

  public saveTeacher(teacher: Teacher) {
    return this.httpService.post(BASE_TEACHER, teacher);
  }

  public saveAccount(teacher: Teacher) {
    return this.httpService.post(BASE_TEACHER_ACCOUNT, teacher);
  }

  private getData() {
    this.alertService.alertLoading('Loading...');
    return this.getTeachers().pipe(
      map((data) => {
        this.alertService.closeAlert();
        this.teachers = data;
      }),
      catchError(err => {
        this.alertService.closeAlert();
        this.router.navigate(['/admin']);
        return of(err);
      })
    );
  }
}
