import {Injectable} from '@angular/core';
import {HttpService} from "./http.service";
import {BASE_CLASS} from "../_constant/path.constant";
import {Classs} from "../_model/classs";
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from "rxjs/index";
import {AlertService} from "./alert.service";
import {catchError, map} from "rxjs/internal/operators";

@Injectable({
  providedIn: 'root'
})
export class ClassesService implements Resolve<any> {
  public classes: Classs[];

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.getData();
  }

  constructor(private httpService: HttpService,
              private alertService: AlertService,
              private router: Router) {
  }

  public getClasses() {
    return this.httpService.get(BASE_CLASS);
  }

  public saveClass(classs: Classs) {
    return this.httpService.post(BASE_CLASS, classs);
  }

  public closeClass(id: string) {
    return this.httpService.put(BASE_CLASS, id);
  }

  private getData() {
    this.alertService.alertLoading('Loading...');
    return this.getClasses().pipe(
      map((data: Classs[]) => {
        this.alertService.closeAlert();
        this.classes = data;
      }),
      catchError(err => {
        this.alertService.closeAlert();
        this.router.navigate(['/admin']);
        return of(err);
      })
    );
  }
}
