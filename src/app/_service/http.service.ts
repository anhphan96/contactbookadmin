import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders(
    {'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) {
  }

  public getWithParam(url, param) {
    return this.http.get(url, {params: param});
  }

  public get(url) {
    return this.http.get(url);
  }

  public post(url, data) {
    return this.http.post(url, data);
  }

  public put(url, data) {
    return this.http.put(url, data);
  }

  public delete(url) {
    return this.http.delete(url);
  }
}
