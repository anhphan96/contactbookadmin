import { Injectable } from '@angular/core';
import {HttpService} from './http.service';
import {AlertService} from './alert.service';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs/index';
import {Parents} from '../_model/parents';
import {BASE_PARENTS, BASE_PARENTS_ACCOUNT} from '../_constant/path.constant';
import {catchError, map} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class ParentsService {
  parentsList: Parents[];

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.getData();
  }
  constructor(private httpService: HttpService,
              private alertService: AlertService,
              private router: Router) { }

  public getParents(): Observable<any> {
    return this.httpService.get(BASE_PARENTS);
  }

  public saveParents(parents: Parents) {
    return this.httpService.post(BASE_PARENTS, parents);
  }

  public saveAccount(parents: Parents) {
    return this.httpService.post(BASE_PARENTS_ACCOUNT, parents);
  }

  private getData() {
    this.alertService.alertLoading('Loading...');
    return this.getParents().pipe(
      map((data) => {
        this.alertService.closeAlert();
        this.parentsList = data;
      }),
      catchError(err => {
        this.alertService.closeAlert();
        this.router.navigate(['/admin']);
        return of(err);
      })
    );
  }
}
