import {Injectable} from '@angular/core';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  public alertError(content) {
    swal.hideLoading();
    const baseOptions = {
      confirmButtonText: 'Close',
      title: 'ERROR!',
      html: content,
      type: 'error',
      timer: 5000,
      heightAuto: false
    };
    return swal((<any>Object).assign(baseOptions)).catch(swal.noop);
  }

  public alertSuccess(content) {
    const baseOptions = {
      title: 'Success!',
      html: content,
      type: 'success',
      timer: 5000,
      heightAuto: false
    };
    return swal((<any>Object).assign(baseOptions)).catch(swal.noop);
  }

  public alertLoading(content) {
    swal.showLoading();
    const baseOptions = {
      title: 'Please wait for second!',
      html: content,
      heightAuto: false,
      allowOutsideClick: false,
      showCancelButton: false,
      showConfirmButton: false
    };
    return swal((<any>Object).assign(baseOptions)).catch(swal.noop);
  }

  public alertConfirmation(content): any {
    const baseOptions = {
      title: 'Are you sure?',
      text: content,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    };
    return swal((<any>Object).assign(baseOptions)).catch(swal.noop);
  }

  public alertConfirmationDelete(content): any {
    const baseOptions = {
      title: 'Are you sure?',
      text: content,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#6c757d',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    };
    return swal((<any>Object).assign(baseOptions)).catch(swal.noop);
  }

  public alertErrorAnswer(content) {
    swal.hideLoading();
    const baseOptions = {
      confirmButtonText: 'Close',
      title: 'Incomplete answers!',
      html: content,
      type: 'warning',
      timer: 5000,
      heightAuto: false
    };
    return swal((<any>Object).assign(baseOptions)).catch(swal.noop);
  }

  public closeAlert() {
    swal.close();
  }
}
