/**
 * Base URL
 * */
export const BASE_BACKEND_URL = 'http://localhost:7070';

export const BASE_TEACHER = BASE_BACKEND_URL + '/teacher';
export const BASE_CLASS = BASE_BACKEND_URL + '/class';
export const BASE_CHILDREN = BASE_BACKEND_URL + '/children';
export const BASE_PARENTS = BASE_BACKEND_URL + '/parents';
export const BASE_TEACHER_ACCOUNT = BASE_TEACHER + '/account';
export const BASE_PARENTS_ACCOUNT = BASE_PARENTS + '/account';

export const QUERY_PARAM = {
  DATE: 'date'
};
