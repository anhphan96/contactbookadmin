import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  DateAdapter, MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import {CdkTableModule} from '@angular/cdk/table';
import {Routes} from '@angular/router';
import {RouterModule} from '@angular/router';
import { TeacherComponent } from './components/teacher/teacher.component';
import {HttpClientModule} from '@angular/common/http';
import {TeacherService} from './_service/teacher.service';
import { CreatedialogComponent } from './components/teacher/createdialog/createdialog.component';
import {FormsModule} from '@angular/forms';
import {DateFormat} from './dateFormat';
import { EditTeacherComponent } from './components/teacher/edit-teacher/edit-teacher.component';
import { ChildrenComponent } from './components/children/children.component';
import { CreateChildrenComponent } from './components/children/create-children/create-children.component';
import { EditChildrenComponent } from './components/children/edit-children/edit-children.component';
import {ChildrenService} from "./_service/children.service";
import {ClasssComponent} from "./components/classs/classs.component";
import {ClassesService} from "./_service/classes.service";
import { CreateClassComponent } from './components/classs/create-class/create-class.component';
import {EditClassComponent} from "./components/classs/edit-class/edit-class.component";
import {initializer} from "./_utils/app-init";
import {KeycloakAngularModule, KeycloakService} from "keycloak-angular";
import { ParentsComponent } from './components/parents/parents.component';
import { CreateParentsComponent } from './components/parents/create-parents/create-parents.component';
import { EditParentsComponent } from './components/parents/edit-parents/edit-parents.component';
import {ParentsService} from "./_service/parents.service";

const routes: Routes = [
  {path: '', redirectTo: '/admin', pathMatch: 'full'},
  {
    path: 'admin', children:
    [
      {
        path: 'teacher',
        component: TeacherComponent,
        resolve: {teachers: TeacherService}
      },
      {
        path: 'children',
        component: ChildrenComponent,
        resolve: {children: ChildrenService}
      },
      {
        path: 'class',
        component: ClasssComponent,
        resolve: {classes: ClassesService}
      },
      {
        path: 'parents',
        component: ParentsComponent,
        resolve: {parents: ParentsService}
      }
    ]
  },
  {path: '**', redirectTo: '/admin', pathMatch: 'full'}
];

const material_module = [
  BrowserAnimationsModule,
  CdkTableModule,
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatStepperModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
];

@NgModule({
  declarations: [
    AppComponent,
    TeacherComponent,
    CreatedialogComponent,
    EditTeacherComponent,
    ChildrenComponent,
    CreateChildrenComponent,
    EditChildrenComponent,
    ClasssComponent,
    CreateClassComponent,
    EditClassComponent,
    ParentsComponent,
    CreateParentsComponent,
    EditParentsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    material_module,
    FormsModule,
    KeycloakAngularModule
  ],
  providers: [{provide: DateAdapter, useClass: DateFormat},
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [KeycloakService]
    }],
  bootstrap: [AppComponent],
  entryComponents: [
    CreatedialogComponent,
    EditTeacherComponent,
    CreateChildrenComponent,
    EditChildrenComponent,
    CreateClassComponent,
    EditClassComponent,
    CreateParentsComponent,
    EditParentsComponent
  ]
})
export class AppModule {
  constructor(private dateAdapter: DateAdapter<MatDatepickerModule>) {
    dateAdapter.setLocale('en-in'); // DD/MM/YYYY
  }
}
