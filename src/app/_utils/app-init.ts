import { KeycloakService } from 'keycloak-angular';
import {KEYCLOAK} from "../_constant/keycloak.constant";

export function initializer(keycloak: KeycloakService): () => Promise<any> {
  return (): Promise<any> => {
    return new Promise(async (resolve, reject) => {
      try {
        await keycloak.init({
          config: {
            url: KEYCLOAK.AUTH_URL,
            realm: KEYCLOAK.REALM,
            clientId: KEYCLOAK.CLIENT_ID
          },
          initOptions: {
            onLoad: 'login-required',
            checkLoginIframe: false
          },
          enableBearerInterceptor: true,
          bearerExcludedUrls: [
            '/assets',
            '/clients/public'
          ],
        });
        resolve();
      } catch (error) {}
    });
  };
}
