import {Component, OnInit} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Teacher} from '../../_model/teacher';
import {TeacherService} from "../../_service/teacher.service";
import {ViewChild} from '@angular/core';
import {CreatedialogComponent} from "./createdialog/createdialog.component";
import {isNullOrUndefined} from "util";
import {AlertService} from "../../_service/alert.service";
import {ClassesService} from "../../_service/classes.service";
import {EditTeacherComponent} from "./edit-teacher/edit-teacher.component";
import * as _ from "lodash";

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {
  teachers: Teacher[];
  displayedColumns: string[] = ['name', 'birthday', 'gender', 'phone', 'classId'];
  dataSource: MatTableDataSource<Teacher>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private teacherService: TeacherService,
              public dialog: MatDialog,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.teachers = this.teacherService.teachers;
    this.dataSource = new MatTableDataSource(this.teachers);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(name: string) {
    this.dataSource.filter = name.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  createTeacherDialog() {
    const dialogRef = this.dialog.open(CreatedialogComponent);

    dialogRef.afterClosed().subscribe((result: Teacher) => {
      if (!isNullOrUndefined(result)) {
        this.teacherService.saveTeacher(result).subscribe((teacher: Teacher) => {
          this.teachers.unshift(teacher);
          this.dataSource = new MatTableDataSource(this.teachers);
          this.dataSource.paginator = this.paginator;
          this.alertService.alertSuccess('Create teacher successfully');
        }, error => {
          this.alertService.alertError('Can not create teacher!');
        });
      }
    });
  }

  editTeacher(id: string) {
    const cloneTeacher = _.cloneDeep(this.teachers.filter(t => t.id === id)[0]);
    const dialogRef = this.dialog.open(EditTeacherComponent, {
      data: cloneTeacher
    });

    dialogRef.afterClosed().subscribe((result: Teacher) => {
      if (!isNullOrUndefined(result)) {
        if (result.isEditing) {
          this.teacherService.saveTeacher(result).subscribe((teacher: Teacher) => {
            const index = this.teachers.indexOf(this.teachers.filter(t => t.id === teacher.id)[0]);
            if (index > -1) {
              this.teachers[index] = teacher;
              this.dataSource = new MatTableDataSource(this.teachers);
              this.dataSource.paginator = this.paginator;
            }
            this.alertService.alertSuccess('Create teacher successfully');
          }, error => {
            this.alertService.alertError('Can not create teacher!');
          });
        } else {
          this.teacherService.saveAccount(result).subscribe((teacher: Teacher) => {
            const index = this.teachers.indexOf(this.teachers.filter(t => t.id === teacher.id)[0]);
            if (index > -1) {
              this.teachers[index] = teacher;
              this.dataSource = new MatTableDataSource(this.teachers);
              this.dataSource.paginator = this.paginator;
            }
            this.alertService.alertSuccess('Save account successfully');
          }, error => {
            console.log(error);
            this.alertService.alertError(error.error);
          });
        }
      }
    });
  }
}
