import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {Teacher} from "../../../_model/teacher";
import {ClassesService} from '../../../_service/classes.service';
import {Classs} from "../../../_model/classs";
import {isNullOrUndefined} from "util";
import {AlertService} from "../../../_service/alert.service";

@Component({
  selector: 'app-createdialog',
  templateUrl: './createdialog.component.html',
  styleUrls: ['./createdialog.component.css']
})
export class CreatedialogComponent implements OnInit {
  public newTeacher: Teacher;
  classes: Classs[];

  constructor(public dialogRef: MatDialogRef<CreatedialogComponent>,
              private classService: ClassesService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.classes = [];
    this.newTeacher = new Teacher();
    this.classService.getClasses().subscribe((data: Classs[]) => {
      this.classes = data;
      this.classes = this.classes.filter(c => c.active);
    }, error => {
      console.log('can not get classes');
    });
  }

  onCancel() {
    this.dialogRef.close();
  }

  onCreateTeacher() {
    if (isNullOrUndefined(this.newTeacher.name) ||
      isNullOrUndefined(this.newTeacher.birthday) ||
      isNullOrUndefined(this.newTeacher.gender) ||
      isNullOrUndefined(this.newTeacher.currentClassId)) {
      this.alertService.alertError('Let fill all information!');
      return;
    }
    this.dialogRef.close(this.newTeacher);
  }

  updateBirthday(date: Date) {
    const dateInfo = date.toString().split('/');
    this.newTeacher.birthday = new Date(dateInfo[2] + '-' + dateInfo[1] + '-' + dateInfo[0]);
  }
}
