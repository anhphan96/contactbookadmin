import {Component, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {Teacher} from "../../../_model/teacher";
import {ClassesService} from "../../../_service/classes.service";
import {AlertService} from "../../../_service/alert.service";
import {Classs} from "../../../_model/classs";
import {isNullOrUndefined} from "util";
import {Inject} from '@angular/core';

@Component({
  selector: 'app-edit-teacher',
  templateUrl: './edit-teacher.component.html',
  styleUrls: ['./edit-teacher.component.css']
})
export class EditTeacherComponent implements OnInit {

  public teacher: Teacher;
  classes: Classs[];
  public isHasAccount: boolean;

  constructor(public dialogRef: MatDialogRef<EditTeacherComponent>,
              private classService: ClassesService,
              private alertService: AlertService,
              @Inject(MAT_DIALOG_DATA) public data: Teacher) {
  }

  ngOnInit() {
    this.classes = [];
    this.teacher = this.data;
    if (isNullOrUndefined(this.teacher.userName)) {
      this.isHasAccount = false;
    } else {
      this.isHasAccount = true;
    }
    this.classService.getClasses().subscribe((data: Classs[]) => {
      this.classes = data;
      this.classes = this.classes.filter(c => c.active);
    }, error => {
      console.log('can not get classes');
    });
  }

  onCancel() {
    this.dialogRef.close();
  }

  onSaveTeacher() {
    if (isNullOrUndefined(this.teacher.name) ||
      isNullOrUndefined(this.teacher.birthday) ||
      isNullOrUndefined(this.teacher.gender) ||
      isNullOrUndefined(this.teacher.currentClassId)) {
      this.alertService.alertError('Let fill all information!');
      return;
    }
    this.teacher.isEditing = true;
    this.dialogRef.close(this.teacher);
  }

  updateBirthday(date: Date) {
    const dateInfo = date.toString().split('/');
    this.teacher.birthday = new Date(dateInfo[2] + '-' + dateInfo[1] + '-' + dateInfo[0]);
  }

  onSaveAccount() {
    if (isNullOrUndefined(this.teacher.userName) ||
      isNullOrUndefined(this.teacher.password) ||
      this.teacher.userName === '' ||
      this.teacher.password === '') {
      this.alertService.alertError('Let fill all information!');
      return;
    }
    this.teacher.isEditing = false;
    this.dialogRef.close(this.teacher);
  }
}
