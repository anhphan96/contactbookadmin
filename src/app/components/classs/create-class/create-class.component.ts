import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material";
import {CreateChildrenComponent} from "../../children/create-children/create-children.component";
import {AlertService} from "../../../_service/alert.service";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-create-class',
  templateUrl: './create-class.component.html',
  styleUrls: ['./create-class.component.css']
})
export class CreateClassComponent implements OnInit {
  public className: string;
  constructor(public dialogRef: MatDialogRef<CreateChildrenComponent>,
              private alertService: AlertService) { }

  ngOnInit() {
  }

  onCreateClass() {
    if (isNullOrUndefined(this.className)) {
      this.alertService.alertError('Let fill the class name');
      return;
    }
    this.dialogRef.close(this.className);
  }

  onCancel() {
    this.dialogRef.close();
  }
}
