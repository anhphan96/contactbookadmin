import {Component, OnInit, ViewChild} from '@angular/core';
import {Classs} from '../../_model/classs';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {ClassesService} from '../../_service/classes.service';
import {AlertService} from '../../_service/alert.service';
import {isNullOrUndefined} from 'util';
import {CreateClassComponent} from "./create-class/create-class.component";
import {EditClassComponent} from "./edit-class/edit-class.component";
import * as _ from 'lodash';

@Component({
  selector: 'app-classs',
  templateUrl: './classs.component.html',
  styleUrls: ['./classs.component.css']
})
export class ClasssComponent implements OnInit {

  classes: Classs[];
  displayedColumns: string[] = ['name', 'createdAt', 'active', 'endedAt', 'close'];
  dataSource: MatTableDataSource<Classs>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private classService: ClassesService,
              public dialog: MatDialog,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.classes = this.classService.classes;
    this.dataSource = new MatTableDataSource(this.classes);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(name: string) {
    this.dataSource.filter = name.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  createClassDialog() {
    const dialogRef = this.dialog.open(CreateClassComponent);

    dialogRef.afterClosed().subscribe((result: string) => {
      if (!isNullOrUndefined(result)) {
        const newCl = new Classs();
        newCl.name = result;
        this.classService.saveClass(newCl).subscribe((classs: Classs) => {
          this.classes.unshift(classs);
          this.dataSource = new MatTableDataSource(this.classes);
          this.dataSource.paginator = this.paginator;
          this.alertService.alertSuccess('Create class successfully');
        }, error => {
          this.alertService.alertError('Can not create class!');
        });
      }
    });
  }

  editClass(id: string) {
    const cloneClass = _.cloneDeep(this.classes.filter(t => t.id === id)[0]);
    const dialogRef = this.dialog.open(EditClassComponent, {
      data: cloneClass
    });

    dialogRef.afterClosed().subscribe((result: Classs) => {
      if (!isNullOrUndefined(result)) {
        this.classService.saveClass(result).subscribe((classs: Classs) => {
          const index = this.classes.indexOf(this.classes.filter(t => t.id === classs.id)[0]);
          if (index > -1) {
            this.classes[index] = classs;
            this.dataSource = new MatTableDataSource(this.classes);
            this.dataSource.paginator = this.paginator;
          }
          this.alertService.alertSuccess('Create teacher successfully');
        }, error => {
          this.alertService.alertError('Can not create teacher!');
        });
      }
    });
  }

  closeClass(id: string) {
    this.alertService.alertConfirmation('Close class?').then(resule => {
      if (resule.value) {
        this.classService.closeClass(id).subscribe(() => {
          const index = this.classes.indexOf(this.classes.filter(c => c.id === id)[0]);
          if (index > -1) {
            this.classes[index].active = false;
            this.dataSource = new MatTableDataSource(this.classes);
            this.dataSource.paginator = this.paginator;
          }
          this.alertService.alertSuccess('class is closed');
        }, error => {
          this.alertService.alertError('Can not close this class!');
        });
      }
    });
  }
}
