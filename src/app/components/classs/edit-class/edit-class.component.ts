import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {AlertService} from "../../../_service/alert.service";
import {Classs} from "../../../_model/classs";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-edit-class',
  templateUrl: './edit-class.component.html',
  styleUrls: ['./edit-class.component.css']
})
export class EditClassComponent implements OnInit {
  classs: Classs;
  constructor(public dialogRef: MatDialogRef<EditClassComponent>,
              private alertService: AlertService,
              @Inject(MAT_DIALOG_DATA) public data: Classs) { }

  ngOnInit() {
    this.classs = this.data;
  }

  onSaveClass() {
    if (isNullOrUndefined(this.classs.name)) {
      this.alertService.alertError('Let fill class name!');
      return;
    }
    this.dialogRef.close(this.classs);
  }

  onCancel() {
    this.dialogRef.close();
  }
}
