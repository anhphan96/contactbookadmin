import {Component, Inject, OnInit} from '@angular/core';
import {Child} from "../../../_model/child";
import {Classs} from "../../../_model/classs";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {ClassesService} from "../../../_service/classes.service";
import {AlertService} from "../../../_service/alert.service";
import {isNullOrUndefined} from "util";
import {ParentsService} from "../../../_service/parents.service";
import {Parents} from "../../../_model/parents";

@Component({
  selector: 'app-edit-children',
  templateUrl: './edit-children.component.html',
  styleUrls: ['./edit-children.component.css']
})
export class EditChildrenComponent implements OnInit {

  public child: Child;
  classes: Classs[];
  parents: Parents[];

  constructor(public dialogRef: MatDialogRef<EditChildrenComponent>,
              private classService: ClassesService,
              private alertService: AlertService,
              private parentsService: ParentsService,
              @Inject(MAT_DIALOG_DATA) public data: Child) {
  }

  ngOnInit() {
    this.classes = [];
    this.child = this.data;
    this.classService.getClasses().subscribe((data: Classs[]) => {
      this.classes = data;
      this.classes = this.classes.filter(c => c.active);
    }, error => {
      console.log('can not get classes');
    });
    this.parentsService.getParents().subscribe((data: Parents[]) => {
      this.parents = data;
    }, error => {
      console.log('can not get parents');
    });
  }

  onCancel() {
    this.dialogRef.close();
  }

  onSaveChild() {
    if (isNullOrUndefined(this.child.name) ||
      isNullOrUndefined(this.child.birthDay) ||
      isNullOrUndefined(this.child.gender) ||
      isNullOrUndefined(this.child.currentClassId)) {
      this.alertService.alertError('Let fill all information!');
      return;
    }
    this.dialogRef.close(this.child);
  }

  updateBirthday(date: Date) {
    const dateInfo = date.toString().split('/');
    this.child.birthDay = new Date(dateInfo[2] + '-' + dateInfo[1] + '-' + dateInfo[0]);
  }

}
