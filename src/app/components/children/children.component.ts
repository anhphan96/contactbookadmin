import {Component, OnInit, ViewChild} from '@angular/core';
import {Child} from "../../_model/child";
import {MatPaginator, MatTableDataSource, MatSort, MatDialog} from "@angular/material";
import {ChildrenService} from "../../_service/children.service";
import {AlertService} from "../../_service/alert.service";
import {CreateChildrenComponent} from "./create-children/create-children.component";
import {isNullOrUndefined} from "util";
import {EditChildrenComponent} from "./edit-children/edit-children.component";
import * as _ from 'lodash';

@Component({
  selector: 'app-children',
  templateUrl: './children.component.html',
  styleUrls: ['./children.component.css']
})
export class ChildrenComponent implements OnInit {

  children: Child[];
  displayedColumns: string[] = ['name', 'birthDay', 'gender', 'currentClassName'];
  dataSource: MatTableDataSource<Child>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private childrenService: ChildrenService,
              public dialog: MatDialog,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.children = this.childrenService.children;
    this.dataSource = new MatTableDataSource(this.children);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(name: string) {
    this.dataSource.filter = name.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  createChildrenDialog() {
    const dialogRef = this.dialog.open(CreateChildrenComponent);

    dialogRef.afterClosed().subscribe((result: Child) => {
      if (!isNullOrUndefined(result)) {
        this.childrenService.saveChild(result).subscribe((child: Child) => {
          this.children.unshift(child);
          this.dataSource = new MatTableDataSource(this.children);
          this.dataSource.paginator = this.paginator;
          this.alertService.alertSuccess('Create teacher successfully');
        }, error => {
          this.alertService.alertError('Can not create teacher!');
        });
      }
    });
  }

  editchild(id: string) {
    const cloneChild = _.cloneDeep(this.children.filter(t => t.id === id)[0]);
    const dialogRef = this.dialog.open(EditChildrenComponent, {
      data: cloneChild
    });

    dialogRef.afterClosed().subscribe((result: Child) => {
      if (!isNullOrUndefined(result)) {
        this.childrenService.saveChild(result).subscribe((child: Child) => {
          const index = this.children.indexOf(this.children.filter(t => t.id === child.id)[0]);
          if (index > -1) {
            this.children[index] = child;
            this.dataSource = new MatTableDataSource(this.children);
            this.dataSource.paginator = this.paginator;
          }
          this.alertService.alertSuccess('Create teacher successfully');
        }, error => {
          this.alertService.alertError('Can not create teacher!');
        });
      }
    });
  }

}
