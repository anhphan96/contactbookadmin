import { Component, OnInit } from '@angular/core';
import {Child} from "../../../_model/child";
import {Classs} from "../../../_model/classs";
import {MatDialogRef} from "@angular/material";
import {ClassesService} from "../../../_service/classes.service";
import {AlertService} from "../../../_service/alert.service";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-create-children',
  templateUrl: './create-children.component.html',
  styleUrls: ['./create-children.component.css']
})
export class CreateChildrenComponent implements OnInit {

  public newChild: Child;
  classes: Classs[];

  constructor(public dialogRef: MatDialogRef<CreateChildrenComponent>,
              private classService: ClassesService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.classes = [];
    this.newChild = new Child();
    this.classService.getClasses().subscribe((data: Classs[]) => {
      this.classes = data;
      this.classes = this.classes.filter(c => c.active);
    }, error => {
      console.log('can not get classes');
    });
  }

  onCancel() {
    this.dialogRef.close();
  }

  onCreateChild() {
    if (isNullOrUndefined(this.newChild.name) ||
      isNullOrUndefined(this.newChild.birthDay) ||
      isNullOrUndefined(this.newChild.gender) ||
      isNullOrUndefined(this.newChild.currentClassId)) {
      this.alertService.alertError('Let fill all information!');
      return;
    }
    this.dialogRef.close(this.newChild);
  }

  updateBirthday(date: Date) {
    const dateInfo = date.toString().split('/');
    this.newChild.birthDay = new Date(dateInfo[2] + '-' + dateInfo[1] + '-' + dateInfo[0]);
  }

}
