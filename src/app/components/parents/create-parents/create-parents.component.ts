import { Component, OnInit } from '@angular/core';
import {Parents} from "../../../_model/parents";
import {MatDialogRef} from "@angular/material";
import {CreatedialogComponent} from "../../teacher/createdialog/createdialog.component";
import {AlertService} from "../../../_service/alert.service";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-create-parents',
  templateUrl: './create-parents.component.html',
  styleUrls: ['./create-parents.component.css']
})
export class CreateParentsComponent implements OnInit {

  public newParents: Parents;

  constructor(public dialogRef: MatDialogRef<CreatedialogComponent>,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.newParents = new Parents();
  }

  onCancel() {
    this.dialogRef.close();
  }

  onCreateParents() {
    if (isNullOrUndefined(this.newParents.name) ||
      isNullOrUndefined(this.newParents.birthDay) ||
      isNullOrUndefined(this.newParents.gender)) {
      this.alertService.alertError('Let fill all information!');
      return;
    }
    this.dialogRef.close(this.newParents);
  }

  updateBirthday(date: Date) {
    const dateInfo = date.toString().split('/');
    this.newParents.birthDay = new Date(dateInfo[2] + '-' + dateInfo[1] + '-' + dateInfo[0]);
  }

}
