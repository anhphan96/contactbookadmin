import {Component, OnInit, ViewChild} from '@angular/core';
import {Parents} from "../../_model/parents";
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {ParentsService} from "../../_service/parents.service";
import {AlertService} from "../../_service/alert.service";
import {CreateParentsComponent} from "./create-parents/create-parents.component";
import {isNullOrUndefined} from "util";
import * as _ from "lodash";
import {EditParentsComponent} from "./edit-parents/edit-parents.component";

@Component({
  selector: 'app-parents',
  templateUrl: './parents.component.html',
  styleUrls: ['./parents.component.css']
})
export class ParentsComponent implements OnInit {

  parentsList: Parents[];
  displayedColumns: string[] = ['name', 'birthday', 'gender', 'phone'];
  dataSource: MatTableDataSource<Parents>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private parentsService: ParentsService,
              public dialog: MatDialog,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.parentsList = this.parentsService.parentsList;
    this.dataSource = new MatTableDataSource(this.parentsList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(name: string) {
    this.dataSource.filter = name.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  createParentsDialog() {
    const dialogRef = this.dialog.open(CreateParentsComponent);

    dialogRef.afterClosed().subscribe((result: Parents) => {
      if (!isNullOrUndefined(result)) {
        this.parentsService.saveParents(result).subscribe((parents: Parents) => {
          this.parentsList.unshift(parents);
          this.dataSource = new MatTableDataSource(this.parentsList);
          this.dataSource.paginator = this.paginator;
          this.alertService.alertSuccess('Create parents successfully');
        }, error => {
          this.alertService.alertError('Can not create parents!');
        });
      }
    });
  }

  editParents(id: string) {
    const cloneParents = _.cloneDeep(this.parentsList.filter(t => t.id === id)[0]);
    const dialogRef = this.dialog.open(EditParentsComponent, {
      data: cloneParents
    });

    dialogRef.afterClosed().subscribe((result: Parents) => {
      if (!isNullOrUndefined(result)) {
        if (result.isEditing) {
          this.parentsService.saveParents(result).subscribe((parents: Parents) => {
            const index = this.parentsList.indexOf(this.parentsList.filter(t => t.id === parents.id)[0]);
            if (index > -1) {
              this.parentsList[index] = parents;
              this.dataSource = new MatTableDataSource(this.parentsList);
              this.dataSource.paginator = this.paginator;
            }
            this.alertService.alertSuccess('Create parents successfully');
          }, error => {
            this.alertService.alertError('Can not create parents!');
          });
        } else {
          this.parentsService.saveAccount(result).subscribe((parents: Parents) => {
            const index = this.parentsList.indexOf(this.parentsList.filter(t => t.id === parents.id)[0]);
            if (index > -1) {
              this.parentsList[index] = parents;
              this.dataSource = new MatTableDataSource(this.parentsList);
              this.dataSource.paginator = this.paginator;
            }
            this.alertService.alertSuccess('Save account successfully');
          }, error => {
            console.log(error);
            this.alertService.alertError(error.error);
          });
        }
      }
    });
  }

}
