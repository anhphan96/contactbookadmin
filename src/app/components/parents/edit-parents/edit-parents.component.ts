import {Component, Inject, OnInit} from '@angular/core';
import {Parents} from "../../../_model/parents";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {AlertService} from "../../../_service/alert.service";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-edit-parents',
  templateUrl: './edit-parents.component.html',
  styleUrls: ['./edit-parents.component.css']
})
export class EditParentsComponent implements OnInit {

  public parents: Parents;
  public isHasAccount: boolean;

  constructor(public dialogRef: MatDialogRef<EditParentsComponent>,
              private alertService: AlertService,
              @Inject(MAT_DIALOG_DATA) public data: Parents) {
  }

  ngOnInit() {
    this.parents = this.data;
    if (isNullOrUndefined(this.parents.userName)) {
      this.isHasAccount = false;
    } else {
      this.isHasAccount = true;
    }
  }

  onCancel() {
    this.dialogRef.close();
  }

  onSaveParents() {
    if (isNullOrUndefined(this.parents.name) ||
      isNullOrUndefined(this.parents.birthDay) ||
      isNullOrUndefined(this.parents.gender)) {
      this.alertService.alertError('Let fill all information!');
      return;
    }
    this.parents.isEditing = true;
    this.dialogRef.close(this.parents);
  }

  updateBirthday(date: Date) {
    const dateInfo = date.toString().split('/');
    this.parents.birthDay = new Date(dateInfo[2] + '-' + dateInfo[1] + '-' + dateInfo[0]);
  }

  onSaveAccount() {
    if (isNullOrUndefined(this.parents.userName) ||
      isNullOrUndefined(this.parents.password) ||
      this.parents.userName === '' ||
      this.parents.password === '') {
      this.alertService.alertError('Let fill all information!');
      return;
    }
    this.parents.isEditing = false;
    this.dialogRef.close(this.parents);
  }

}
